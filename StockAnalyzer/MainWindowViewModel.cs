﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockAnalyzer
{
    public class MainWindowViewModel
    {
        private UnbiastockParser parser = new UnbiastockParser();

        public String SelectedType { get; set; }
        public String SelectedInterval { get; set; }

        private double w6 = 100;
        public Double W6
        {
            get
            {
                return w6;
            }
            set
            {
                w6 = value;
                CalculateBest();
            }
        }

        private double w12 = 80;
        public Double W12
        {
            get
            {
                return w12;
            }
            set
            {
                w12 = value;
                CalculateBest();
            }
        }

        private double w24 = 60;
        public Double W24
        {
            get
            {
                return w24;
            }
            set
            {
                w24 = value;
                CalculateBest();
            }
        }

        private double w48 = 40;
        public Double W48
        {
            get
            {
                return w48;
            }
            set
            {
                w48 = value;
                CalculateBest();
            }
        }

        private double w168 = -100;
        public Double W168
        {
            get
            {
                return w168;
            }
            set
            {
                w168 = value;
                CalculateBest();
            }
        }

        private ObservableCollection<String> types = new ObservableCollection<String>();
        public ObservableCollection<String> Types
        {
            get
            {
                return types;
            }
            set
            {
                types = value;
            }
        }

        private ObservableCollection<String> interval = new ObservableCollection<String>();
        public ObservableCollection<String> Interval
        {
            get
            {
                return interval;
            }
            set
            {
                interval = value;
            }
        }


        private ObservableCollection<Entry> data = new ObservableCollection<Entry>();
        public ObservableCollection<Entry> Data
        {
            get
            {
                return data;
            }
            set
            {
                data = value;
            }
        }

        private ObservableCollection<BestEntry> best = new ObservableCollection<BestEntry>();
        public ObservableCollection<BestEntry> Best
        {
            get
            {
                return best;
            }
            set
            {
                best = value;
            }
        }

        public Entry SelectedItem { get; set; }

        public MainWindowViewModel()
        {
            Types.Add("wallstreetbets");
            Types.Add("StockMarket");
            Types.Add("pennystocks");
            Types.Add("investing");
            Types.Add("stocks");
            Types.Add("RobinHoodPennyStocks");
            Types.Add("Daytrading");
            Types.Add("SPACs");
            Types.Add("weedstocks");
            Types.Add("CanadianInvestor");
            Types.Add("smallstreetbets");

            //Interval.Add("1");
            Interval.Add("6");
            Interval.Add("12");
            Interval.Add("24");
            Interval.Add("48");
            Interval.Add("168");

            SelectedType = Types[0];
            SelectedInterval = Interval[0];

            ColorManager.Load();
        }

        public void ParseUnbiastock()
        {
            Data.Clear();
            foreach (Entry entry in parser.GetList(SelectedType, SelectedInterval))
                Data.Add(entry);
        }

        public void CalculateBest()
        {
            Best.Clear();
            foreach (BestEntry entry in parser.GetBest(SelectedType, W6, W12, W24, W48, W168))
                Best.Add(entry);
        }

        public void ClearCache()
        {
            this.parser.ClearCache();
            this.ParseUnbiastock();
            this.CalculateBest();
        }
    }
}

