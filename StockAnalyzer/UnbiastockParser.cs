﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace StockAnalyzer
{
    public class UnbiastockParser
    {
        private static String BASE_URL = "https://unbiastock.com/TableReddit.php";
        private static String PARAMS_COMPARE = "?compare2=";
        private static String PARAMS_COMPARE_SECTOR = "&compare_sector=";
        private static String PARAMS_MAIL_NEWS = "&mailnews=";

        private Dictionary<String, List<Entry>> cache = new Dictionary<string, List<Entry>>();

        public List<Entry> GetList(String type, String interval)
        {
            String key = Key(type, interval);
            if (this.cache.ContainsKey(key))
            {
                Console.WriteLine("Got \"" + key + "\" from cache");
                return this.cache[key];
            }
            String url = BASE_URL + PARAMS_COMPARE + type + PARAMS_COMPARE_SECTOR + interval + PARAMS_MAIL_NEWS;
            Console.WriteLine("Request: \"" + url + "\"");
            HtmlDocument doc = new HtmlWeb().Load(url);
            HtmlNode tbody = doc.DocumentNode.Descendants("tbody").First();
            if (tbody != null)
            {
                HtmlNodeCollection nodes = tbody.SelectNodes("tr");
                if (nodes != null)
                {
                    this.cache[key] = new List<Entry>(nodes.Select(x => ParseEntry(x)));
                    return this.cache[key];
                }
            }
            return new List<Entry>();
        }

        private static String Key(String type, String interval)
        {
            return type + ":" + interval;
        }

        public void ClearCache()
        {
            this.cache.Clear();
        }

        private Entry ParseEntry(HtmlNode element)
        {
            Entry result = new Entry();
            result.Ticker = element.ChildNodes[0].ChildNodes[1].InnerHtml;
            result.URL = GetURL(element);
            result.RedditScore = ParseInteger(element.ChildNodes[1].InnerHtml);
            result.PrevScore = ParseInteger(element.ChildNodes[2].InnerHtml);
            result.ScoreChange = ParseDouble(element.ChildNodes[3].InnerHtml);
            result.Rockets = ParseInteger(element.ChildNodes[4].InnerHtml);
            result.StockPrice = ParseDouble(element.ChildNodes[5].InnerHtml);
            result.PrevClose = ParseDouble(element.ChildNodes[6].InnerHtml);
            result.Change = ParseDouble(element.ChildNodes[7].InnerHtml);
            result.Shorted = ParseDouble(element.ChildNodes[8].InnerHtml);
            result.Volume = ParseDouble(element.ChildNodes[9].InnerHtml);
            return result;
        }

        private static String GetURL(HtmlNode element)
        {
            try
            {
                return element.Descendants("a").First().Attributes["href"].Value;
            }
            catch(Exception e)
            {
            }
            return null;
        }

        public IEnumerable<BestEntry> GetBest(String type, Double w6, Double w12, Double w24, Double w48, Double w168)
        {
             Dictionary<String, BestEntry> temp = new Dictionary<string, BestEntry>();
             try
             {
                 GetBest(temp, type, "168");
                 GetBest(temp, type, "48");
                 GetBest(temp, type, "24");
                 GetBest(temp, type, "12");
                 GetBest(temp, type, "6");
             }
             catch (Exception e)
             {
                 Console.Error.Write(e);
             }
             List<BestEntry> result = new List<BestEntry>(temp.Values);
             foreach (BestEntry entry in result)
                 entry.CalculateScore(w6, w12, w24, w48, w168);

             result.Sort((e1, e2) => e2.Score.CompareTo(e1.Score));
             for (int i = 0; i < result.Count; i++)
                 result[i].Place = i + 1;

            /*List<BestEntry> result = new List<BestEntry>();
            BestEntry temp = new BestEntry("Test");

            temp.PlaceIn6 = 1;
            temp.PlaceIn12 = 1;
            temp.PlaceIn24= 1;
            temp.PlaceIn48 = 1;
            temp.PlaceIn168 = 1;

            temp.ScoreChange6 = 50;
            temp.ScoreChange12 = 50;
            temp.ScoreChange24 = 50;
            temp.ScoreChange48 = 50;
            temp.ScoreChange168 = 50;

            temp.CalculateScore(w6, w12, w24, w48, w168);

            result.Add(temp);*/
            return result;
        }

        private void GetBest(Dictionary<String, BestEntry> map, String type, String interval)
        {
            List<Entry> entries = GetList(type, interval);
            entries.Sort((e1, e2) => e2.ScoreChange.CompareTo(e1.ScoreChange));
            for (int i = 0; i < entries.Count; i++)
                Add(map, entries[i], interval, i + 1);
        }

        private static void Add(Dictionary<String, BestEntry> map, Entry entry, String interval, Int32 place)
        {
            if (!map.ContainsKey(entry.Ticker))
                map[entry.Ticker] = new BestEntry(entry.Ticker, entry.URL);
            BestEntry bestEntry = map[entry.Ticker];

            if (interval == "6")
            {
                bestEntry.PlaceIn6 = place;
                bestEntry.ScoreChange6 = entry.ScoreChange;
            }

            if (interval == "12")
            {
                bestEntry.PlaceIn12 = place;
                bestEntry.ScoreChange12 = entry.ScoreChange;
            }

            if (interval == "24")
            {
                bestEntry.PlaceIn24 = place;
                bestEntry.ScoreChange24 = entry.ScoreChange;
            }

            if (interval == "48")
            {
                bestEntry.PlaceIn48 = place;
                bestEntry.ScoreChange48 = entry.ScoreChange;
            }

            if (interval == "168")
            {
                bestEntry.PlaceIn168 = place;
                bestEntry.ScoreChange168 = entry.ScoreChange;
            }
        }

        private static Int32 ParseInteger(String source)
        {
            return Int32.Parse(clear(source));
        }

        private static Double ParseDouble(String source)
        {
            return Double.Parse(clear(source));
        }

        private static String clear(String source)
        {
            source = source.Replace("M$", "");
            source = source.Replace("$", "");
            source = source.Replace("%", "");
            source = source.Replace(".", ",");
            return source;
        }
    }
}
