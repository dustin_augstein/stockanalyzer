﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace StockAnalyzer
{
    public class BestEntry : INotifyPropertyChanged
    {
        private static int NO_PLACE = -1;

        public Int32 Place { get; set; }
        public String Ticker { get; set; }
        public String URL { get; set; }

        public Double ScoreChange6 { get; set; }
        public Double ScoreChange12 { get; set; }
        public Double ScoreChange24 { get; set; }
        public Double ScoreChange48 { get; set; }
        public Double ScoreChange168 { get; set; }

        public Int32 PlaceIn6 { get; set; } = NO_PLACE;
        public Int32 PlaceIn12 { get; set; } = NO_PLACE;
        public Int32 PlaceIn24 { get; set; } = NO_PLACE;
        public Int32 PlaceIn48 { get; set; } = NO_PLACE;
        public Int32 PlaceIn168 { get; set; } = NO_PLACE;

        public bool InSetOf6 { get { return PlaceIn6 != NO_PLACE; } }
        public bool InSetOf12 { get { return PlaceIn12 != NO_PLACE; } }
        public bool InSetOf24 { get { return PlaceIn24 != NO_PLACE; } }
        public bool InSetOf48 { get { return PlaceIn48 != NO_PLACE; } }
        public bool InSetOf168 { get { return PlaceIn168 != NO_PLACE; } }

        private Int32 score;
        public Int32 Score
        {
            get
            {
                return this.score;
            }
        }

        public Brush BackgroundColor
        {
            get
            {
                return ColorManager.GetBrush(Ticker);
            }
            set
            {
                ColorManager.SetBrush(Ticker, value);
                NotifyBackgroundChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void CalculateScore(Double w6, Double w12, Double w24, Double w48, Double w168)
        {
            this.score = (Int32)(valuate(PlaceIn6, w6) + valuate(PlaceIn12, w12) + valuate(PlaceIn24, w24) + valuate(PlaceIn48, w48) + valuate(PlaceIn168, w168) +
                   (ScoreChange6 / 100) * w6 + (ScoreChange12 / 100) * w12 + (ScoreChange24 / 100) * w24 + (ScoreChange48 / 100) * w48 + (ScoreChange168 / 100) * w168);
        }

        private static Double valuate(Int32 place, Double weight)
        {
            return 1.0 / place * weight;
        }

        public BestEntry(String ticker, String url)
        {
            Ticker = ticker;
            URL = url;
        }

        public void NotifyBackgroundChanged()
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(BackgroundColor)));
        }
    }
}

