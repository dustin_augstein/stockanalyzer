﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace StockAnalyzer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindowViewModel ViewModel
        {
            get; set;
        }

        public MainWindow()
        {
            InitializeComponent();
            ViewModel = new MainWindowViewModel();
            DataContext = ViewModel;
            Console.Out.WriteLine("Test");
        }

        private void cmbType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ViewModel.ParseUnbiastock();
            ViewModel.CalculateBest();
        }

        private void cmbInteval_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ViewModel.ParseUnbiastock();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.ClearCache();
        }

        private static readonly Regex _regex = new Regex("[^0-9.-]+"); //regex that matches disallowed text
        private static bool IsTextAllowed(string text)
        {
            return !_regex.IsMatch(text);
        }

        private void TextBlock_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (e.Text != null)
                e.Handled = !IsTextAllowed(e.Text);
        }

        private void DG_Hyperlink_Click(object sender, RoutedEventArgs e)
        {
            Hyperlink link = (Hyperlink)e.OriginalSource;
            Process.Start(link.NavigateUri.AbsoluteUri);
        }

        private void ClrPcker_Background_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color?> e)
        {
            Console.Out.WriteLine();
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {

            ColorPickerDialog colorDialog = new ColorPickerDialog(ColorManager.GetColor(ViewModel.SelectedItem.Ticker));
            if (colorDialog.ShowDialog() == true)
            {
                ViewModel.SelectedItem.BackgroundColor = new SolidColorBrush(colorDialog.Color);
                foreach(BestEntry best in ViewModel.Best)
                {
                    if (best.Ticker == ViewModel.SelectedItem.Ticker)
                        best.NotifyBackgroundChanged();
                }
            }
        }
    }
}
