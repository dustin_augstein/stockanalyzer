﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace StockAnalyzer
{
    /// <summary>
    /// Interaction logic for ColorPicker1.xaml
    /// </summary>
    public partial class ColorPickerDialog : Window
    {
        public Color Color { get; set; }

        public ColorPickerDialog(Color color)
        {
            InitializeComponent();
            Color = color;
            DataContext = this;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}
