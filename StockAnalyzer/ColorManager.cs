﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace StockAnalyzer
{
    public class ColorManager
    {
        private static Dictionary<string, Brush> ColorMap { get; } = new Dictionary<string, Brush>();

        public static Brush GetBrush(String ticker)
        {
            if (ColorMap.ContainsKey(ticker))
                return ColorMap[ticker];
            return null;
        }

        public static void SetBrush(String ticker, Brush brush)
        {
            ColorMap[ticker] = brush;
            Save();
        }

        public static Color GetColor(String ticker)
        {
            Brush brush = GetBrush(ticker);
            if (brush != null)
                return ((SolidColorBrush)brush).Color;
            return Colors.Transparent;
        }

        private static String COLOR_FILE = "color.csv";

        private static void Save()
        {
            if (!File.Exists(COLOR_FILE))
                File.Create(COLOR_FILE);
            StreamWriter writer = new StreamWriter(COLOR_FILE);
            foreach (String ticker in ColorMap.Keys)
            {
                Color color = GetColor(ticker);
                byte a = color.A;
                byte r = color.R;
                byte g = color.G;
                byte b = color.B;
                writer.WriteLine(ticker + "," + a + "," + r + "," + g + "," + b);
            }
            writer.Close();
        }

        public static void Load()
        {
            if (!File.Exists(COLOR_FILE))
                return;
            StreamReader reader = new StreamReader(COLOR_FILE);
            while (!reader.EndOfStream)
            {
                String line = reader.ReadLine();
                String[] parts = line.Split(',');
                String ticker = parts[0];
                byte a = byte.Parse(parts[1]);
                byte r = byte.Parse(parts[2]);
                byte g = byte.Parse(parts[3]);
                byte b = byte.Parse(parts[4]);
                Color color = Color.FromArgb(a, r, g, b);
                ColorMap[ticker] = new SolidColorBrush(color);
            }
            reader.Close();
        }
    }
}
