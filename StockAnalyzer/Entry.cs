﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace StockAnalyzer
{
    public class Entry : INotifyPropertyChanged
    {
        public String Ticker { get; set; }
        public String URL { get; set; }
        public Int32 RedditScore { get; set; }
        public Int32 PrevScore { get; set; }
        public Double ScoreChange { get; set; }
        public Int32 Rockets { get; set; }
        public Double StockPrice { get; set; }
        public Double PrevClose { get; set; }
        public Double Change { get; set; }
        public Double Shorted { get; set; }
        public Double Volume { get; set; }

        public Brush BackgroundColor
        {
            get
            {
                return ColorManager.GetBrush(Ticker);
            }
            set
            {
                ColorManager.SetBrush(Ticker, value);
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(BackgroundColor)));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
